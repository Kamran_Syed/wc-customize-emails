<?php
	global $aspk_wc_emails;
	$default_array = $aspk_wc_emails->thankyou_email_settings();
	$option = get_option('aspk_reset_email_setting',$default_array);
	alog('$option',$option,__FILE__,__LINE__);
	
	$html_codes =  html_entity_decode(stripslashes($option['new_mail_order']), 2, "UTF-8");
	alog('$blogname222',get_permalink( wc_get_page_id( 'myaccount' ) ),__FILE__,__LINE__);
	$html_codes = str_replace("[{USERNAME}]",$user_login,$html_codes);
	$html_codes = str_replace("[{RESETPASSLINK}]",'<a href='.esc_url( add_query_arg( array( 'key' => $reset_key, 'login' => rawurlencode( $user_login ) ), wc_get_endpoint_url( 'lost-password', '', get_permalink( wc_get_page_id( 'myaccount' ) ) ) ) ).'>Click here to reset your password</a>',$html_codes);
	echo $html_codes;
?>