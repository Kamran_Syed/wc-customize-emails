<?php
/*
Plugin Name:wc_customize_emails
Plugin URI: 
Description:wc_customize_emails
Version: 1.1
Author: AgileSolutionspk.com
Author URI: http://agilesolutionspk.com/
*/
error_reporting(E_ALL);
ini_set('display_errors', 1);
if ( !class_exists( 'aspk_wc_customize_emails' )){   
	class aspk_wc_customize_emails{
	
			function __construct() {
				register_activation_hook( __FILE__, array($this, 'install') );
				add_action( 'admin_menu', array(&$this, 'Email_page_register') );
				add_action('admin_enqueue_scripts', array(&$this, 'wp_enqueue_scripts'));
				
				add_filter( 'woocommerce_email_style_inline_tags', array( $this, 'style_inline_tags' ),99,2 );/////////Ok
				add_filter('woocommerce_email_recipient_new_order', array( $this,'wc_change_admin_new_order_email_recipient'), 9999, 2);///////////Ok
				add_filter('woocommerce_email_subject_new_order', 'wc_change_admin_new_order_email_subject', 9999, 2);
				add_filter( 'phpmailer_init', array( $this, 'handle_multipart' ),1 );///////////// Ok
				
				add_filter('woocommerce_locate_template', array(&$this,'replace_woo_templates'),1,3);
				
			}
						
			function replace_woo_templates($template, $template_name, $template_path){
			alog('$template'.$template_name,$template_name,__FILE__,__LINE__);
				if($template_name == 'emails/customer-invoice.php'){
					$default_array = $this->thankyou_email_settings();
					$thank_u_setting = get_option('aspk_invoice_email_setting', $default_array);
					$check = $thank_u_setting['check_field'];
					if($check == 'on'){
						$newtemp = __DIR__ .'/replaced_invoice_email.php';
						return $newtemp;
					}				
				}
			
			
				if($template_name == 'emails/customer-processing-order.php'){
					$default_array = $this->thankyou_email_settings();
					$thank_u_setting = get_option('aspk_my_template_setting', $default_array);
					$check = $thank_u_setting['check_field'];
					if($check == 'on'){
						$newtemp = __DIR__ .'/replaced_order_thankyou_email.php';
						return $newtemp;
					}				
				}
				if($template_name == 'emails/customer-completed-order.php'){
					$default_array = $this->thankyou_email_settings();
					$thank_u_setting = get_option('aspk_my_order_completed_temp_settings', $default_array);	
					$check = $thank_u_setting['check_field'];
					if($check == 'on'){
						$newtemp = __DIR__ .'/replaced_order_completed_email.php';
						return $newtemp;
					}	
							
				}
				if($template_name == 'emails/customer-processing-order.php'){
					$default_array = $this->thankyou_email_settings();
					$thank_u_setting = get_option('aspk_processing_email_setting', $default_array);
					$check = $thank_u_setting['check_field'];
					if($check == 'on'){
						$newtemp = __DIR__ .'/replaced_proceesing_order_email.php';
						return $newtemp;
					}	
						
				}
				if($template_name == 'emails/customer-new-account.php'){
					$default_array = $this->thankyou_email_settings();
					$thank_u_setting = get_option('aspk_new_account_email_setting', $default_array);
					$check = $thank_u_setting['check_field'];
					if($check == 'on'){
						$newtemp = __DIR__ .'/replaced_customer_new_account_email.php';
						return $newtemp;
					}	
						
				}
				if($template_name == 'emails/customer-reset-password.php'){
					$default_array = $this->thankyou_email_settings();
					$thank_u_setting = get_option('aspk_reset_email_setting', $default_array);
					alog('$reset',$thank_u_setting,__FILE__,__LINE__);
					$check = $thank_u_setting['check_field'];
					if($check == 'on'){
						$newtemp = __DIR__ .'/replaced_reset_password_email.php';
						return $newtemp;
					}	
						
				}	
				if($template_name == 'emails/customer-note.php'){
					$default_array = $this->thankyou_email_settings();
					$thank_u_setting = get_option('aspk_note_template_setting', $default_array);
					alog('$reset',$thank_u_setting,__FILE__,__LINE__);
					$check = $thank_u_setting['check_field'];
					if($check == 'on'){
						$newtemp = __DIR__ .'/replaced_customer_note.php';
						return $newtemp;
					}	
						
				}			
				
			  return $template; 
			}
		
			function handle_multipart($mailer){/////////ok
			}
			
			
			function wc_change_admin_new_order_email_recipient($recipient, $order){////////////ok
				global $woocommerce;
				$thank_u_setting = get_option('aspk_my_template_setting');
				$email = $thank_u_setting['new_ord_recp'];
				$check = $thank_u_setting['check_field'];
				if($check == 'on'){	
					$recipient =  $email;
				}
				alog('$recipient',$recipient,__FILE__,__LINE__);
				return $recipient;
			}
			
			function wc_change_admin_new_order_email_subject($subject, $order){////////////ok
				alog('($subject',$subject,__FILE__,__LINE__);
				global $woocommerce;
				$thank_u_setting = get_option('aspk_my_template_setting');
				$email = $thank_u_setting['new_ord_sub'];
				$check = $thank_u_setting['check_field'];
				if($check == 'on'){	
					return $email;
				}		
				
			}
			
			function style_inline_tags($tags){////////////ok
			}
				
			function aspk_new_order(){
		
				remove_action( 'media_buttons', 'media_buttons');	
				$editor_id = 'new_mail_order';
				//ob_start();
				require_once('default_order_thankyou_email.php');
				$new_order_temp = ob_get_contents();
				//ob_end_clean();
				wp_editor( $new_order_temp, $editor_id );
			}
			
			
			
			function aspk_order_comp(){	
				
				$editor_id = 'order_comp';
				$order_comp= file_get_contents(__DIR__.'/default_order_completed_email.php');
				wp_editor( $order_comp, $editor_id );
			}
			
			function processing_order_editor(){
				$editor_id = 'pro_mail_order';
				$new_order_temp =file_get_contents(__DIR__.'/default_order_processing_email.php');
				wp_editor( $new_order_temp, $editor_id );
			}
			
			function invoice_order_editor(){
				$editor_id = 'invoice_mail_order';
				$new_order_temp =file_get_contents(__DIR__.'/default_customer_invoice_email.php');
				wp_editor( $new_order_temp, $editor_id );
			}
			
			function new_account_order_editor(){
				$editor_id = 'new_account_mail_order';
				$new_order_temp =file_get_contents(__DIR__.'/default_customer_new_account_email.php');
				wp_editor( $new_order_temp, $editor_id );
			}
			
			function reset_password_editor(){
				$editor_id = 'reset_pass_editor';
				$new_order_temp =file_get_contents(__DIR__.'/default_resset_customer_pass_email.php');
				wp_editor( $new_order_temp, $editor_id );
			}
			
			function customer_note_editor(){
				$editor_id = 'customer_note_editor';
				$new_order_temp =file_get_contents(__DIR__.'/default_customer_note_email.php');
				wp_editor( $new_order_temp, $editor_id );
			}
			
			function install(){
			
			}
			
			function wp_enqueue_scripts(){
				wp_enqueue_script('jquery');
				wp_enqueue_script('jquery-ui-tabs');
				wp_enqueue_style( 'jq_cor_cs', plugins_url('css/jquery-core.css', __FILE__) );
				wp_enqueue_style( 'tw-bs', plugins_url('css/tw-bs.3.1.1.css', __FILE__) );
			}
			
			function Email_page_register() {
				add_submenu_page( 'woocommerce', 'Customize Emails', 'Customize Emails', 'manage_options', 'aspk_customize_mails',array(&$this, 'Customize_Emails')); 
			}
			
			function def_setting(){
			
				$default = array(
				  'first_name'=>'DON',
				  'last_name'=>'JOE',
				  'email'=>'JOE@gmail.com',
				  'order_no'=>'123',
				  'amount'=>'$200',  
				  'date'=>'jan-2015',
				  'company'=>'software house',
				  'address'=>'ontario',
				  'city'=>'ontario', 
				  'country'=>'canada',
				  'state'=>'toronto',
				  'zip_code'=>'2222',
				  'phone_no'=>'04412345678',
				  'Product'=>'mobile',
				  'Quantity'=>'5',
				  'price'=>'$50',
				  'Cart_subtotal'=>'$500', 
				  'Shipping'=>'free',
				  'Payment_method'=>'paypal',
				  'Order_total'=>'$700',
				  'user_name'=>'andrew',
				  'Link'=>'click on link',
				  'note'=>'hey user',
				  'pay_Link'=>'click on link to pay order payment',
				  'rest_link'=>'click on link to reset your password',
				  
				);	
				return $default;
			}
			
			function thankyou_email_settings(){
			
				$email_temp = array();
				$email_temp['new_mail_order'] = '';
				$email_temp['new_ord_sub'] = '';
				$email_temp['new_ord_recp'] ='';
				$email_temp['check_field'] = '';
				return $email_temp;
			}
			
			function replace_note_email(){
			
				$default_setting = $this->def_setting();
				$default = $this->thankyou_email_settings();
				$option = get_option('aspk_note_template_setting',$default);
				alog('$option5555',$option,__FILE__,__LINE__);
				$html_codes =  html_entity_decode(stripslashes($option['new_mail_order']), 2, "UTF-8");
				alog('$html_codessadsdsds',$html_codes,__FILE__,__LINE__);
				$html_codes = str_replace("[{NOTE}]",$default_setting['note'],$html_codes);	$html_codes = str_replace("[{ORDERNO}]",$default_setting['order_no'],$html_codes);
				$html_codes = str_replace("[{EMAIL}]",$default_setting['email'],$html_codes);
				$html_codes = str_replace("[{TELENO}]",$default_setting['phone_no'],$html_codes);
				///////////////////////////////////////////////////////////////////////////
				$html_codes = str_replace("[{BFIRSTNAME}]",$default_setting['first_name'],$html_codes);
				$html_codes = str_replace("[{BLASTNAME}]",$default_setting['last_name'],$html_codes);
				$html_codes = str_replace("[{BCOMPANYNAME}]",$default_setting['company'],$html_codes);
				$html_codes = str_replace("[{BADDRESS}]",$default_setting['address'],$html_codes);
				$html_codes = str_replace("[{BTOWNCITY}]",$default_setting['city'],$html_codes);
				$html_codes = str_replace("[{BZIPCODE}]",$default_setting['zip_code'],$html_codes);
				$html_codes = str_replace("[{BCOUNTRY}]",$default_setting['country'],$html_codes);
				$html_codes = str_replace("[{BSTATE}]",$default_setting['state'],$html_codes);
				//////////////////////////////////////////////////////////////////////////

				$html_codes = str_replace("[{SFIRSTNAME}]",$default_setting['first_name'],$html_codes);
				$html_codes = str_replace("[{SLASTNAME}]",$default_setting['last_name'],$html_codes);
				$html_codes = str_replace("[{SCOMPANYNAME}]",$default_setting['company'],$html_codes);
				$html_codes = str_replace("[{SADDRESS}]",$default_setting['address'],$html_codes);
				$html_codes = str_replace("[{STOWNCITY}]",$default_setting['city'],$html_codes);
				$html_codes = str_replace("[{SZIPCODE}]",$default_setting['zip_code'],$html_codes);
				$html_codes = str_replace("[{SCOUNTRY}]",$default_setting['country'],$html_codes);
				$html_codes = str_replace("[{PRODUCT}]",$default_setting['Product'],$html_codes);
				$html_codes = str_replace("[{QUNTITY}]",$default_setting['Quantity'],$html_codes);
				$html_codes = str_replace("[{PRICE}]",$default_setting['price'],$html_codes);
				$html_codes = str_replace("[{CARTSUBTOTAL}]",$default_setting['Cart_subtotal'],$html_codes);
				$html_codes = str_replace("[{SHIPPING}]",$default_setting['Shipping'],$html_codes);
				$html_codes = str_replace(" [{SSTATE}]",$default_setting['state'],$html_codes);
				$html_codes = str_replace("[{PAYMENTMETHOD}]",$default_setting['Payment_method'],$html_codes);
				$html_codes = str_replace("[{ORDERTOTAL}]",$default_setting['Order_total'],$html_codes);
				return $html_codes;
			}
			
			function replace_tempalte(){
			
				$default_setting = $this->def_setting();
				$default = $this->thankyou_email_settings();
				$option = get_option('aspk_my_template_setting',$default);
				alog('$option5555',$option,__FILE__,__LINE__);
				$html_codes =  html_entity_decode(stripslashes($option['new_mail_order']), 2, "UTF-8");
				alog('$html_codessadsdsds',$html_codes,__FILE__,__LINE__);
				$html_codes = str_replace("[{ORDERNO}]",$default_setting['order_no'],$html_codes);
				$html_codes = str_replace("[{EMAIL}]",$default_setting['email'],$html_codes);
				$html_codes = str_replace("[{TELENO}]",$default_setting['phone_no'],$html_codes);
				///////////////////////////////////////////////////////////////////////////
				$html_codes = str_replace("[{BFIRSTNAME}]",$default_setting['first_name'],$html_codes);
				$html_codes = str_replace("[{BLASTNAME}]",$default_setting['last_name'],$html_codes);
				$html_codes = str_replace("[{BCOMPANYNAME}]",$default_setting['company'],$html_codes);
				$html_codes = str_replace("[{BADDRESS}]",$default_setting['address'],$html_codes);
				$html_codes = str_replace("[{BTOWNCITY}]",$default_setting['city'],$html_codes);
				$html_codes = str_replace("[{BZIPCODE}]",$default_setting['zip_code'],$html_codes);
				$html_codes = str_replace("[{BCOUNTRY}]",$default_setting['country'],$html_codes);
				$html_codes = str_replace("[{BSTATE}]",$default_setting['state'],$html_codes);
				//////////////////////////////////////////////////////////////////////////

				$html_codes = str_replace("[{SFIRSTNAME}]",$default_setting['first_name'],$html_codes);
				$html_codes = str_replace("[{SLASTNAME}]",$default_setting['last_name'],$html_codes);
				$html_codes = str_replace("[{SCOMPANYNAME}]",$default_setting['company'],$html_codes);
				$html_codes = str_replace("[{SADDRESS}]",$default_setting['address'],$html_codes);
				$html_codes = str_replace("[{STOWNCITY}]",$default_setting['city'],$html_codes);
				$html_codes = str_replace("[{SZIPCODE}]",$default_setting['zip_code'],$html_codes);
				$html_codes = str_replace("[{SCOUNTRY}]",$default_setting['country'],$html_codes);
				$html_codes = str_replace("[{PRODUCT}]",$default_setting['Product'],$html_codes);
				$html_codes = str_replace("[{QUNTITY}]",$default_setting['Quantity'],$html_codes);
				$html_codes = str_replace("[{PRICE}]",$default_setting['price'],$html_codes);
				$html_codes = str_replace("[{CARTSUBTOTAL}]",$default_setting['Cart_subtotal'],$html_codes);
				$html_codes = str_replace("[{SHIPPING}]",$default_setting['Shipping'],$html_codes);
				$html_codes = str_replace(" [{SSTATE}]",$default_setting['state'],$html_codes);
				$html_codes = str_replace("[{PAYMENTMETHOD}]",$default_setting['Payment_method'],$html_codes);
				$html_codes = str_replace("[{ORDERTOTAL}]",$default_setting['Order_total'],$html_codes);
				return $html_codes;

				
			}
			
			function replace_new_account_tempalte(){
			
				$default_setting = $this->def_setting();
				$default = $this->thankyou_email_settings();
				$option = get_option('aspk_new_account_email_setting',$default);
				$html_codes =  html_entity_decode(stripslashes($option['new_mail_order']), 2, "UTF-8");
				
				$html_codes = str_replace("[{USERNAME}]",$default_setting['user_name'],$html_codes);
				$html_codes = str_replace("[{LINK}]",$default_setting['Link'],$html_codes);	
				return $html_codes;
				
			}
			
			function replace_reset_password_tempalte(){
			
				$default_setting = $this->def_setting();
				$default = $this->thankyou_email_settings();
				$option = get_option('aspk_reset_email_setting',$default);
				$html_codes =  html_entity_decode(stripslashes($option['new_mail_order']), 2, "UTF-8");
				
				$html_codes = str_replace("[{USERNAME}]",$default_setting['user_name'],$html_codes);
				$html_codes = str_replace("[{RESETPASSLINK}]",$default_setting['rest_link'],$html_codes);	
				return $html_codes;
			}
			
			function replaced_order_compeleted_email_tempalte(){
			
				$default_setting = $this->def_setting();
				$default = $this->thankyou_email_settings();
				$option = get_option('aspk_my_order_completed_temp_settings',$default);
				$html_codes =  html_entity_decode(stripslashes($option['new_mail_order']), 2, "UTF-8");
				$html_codes = str_replace("[{ORDERNO}]",$default_setting['order_no'],$html_codes);
				$html_codes = str_replace("[{EMAIL}]",$default_setting['email'],$html_codes);
				$html_codes = str_replace("[{TELENO}]",$default_setting['phone_no'],$html_codes);
				$html_codes = str_replace("[{BFIRSTNAME}]",$default_setting['first_name'],$html_codes);
				$html_codes = str_replace("[{BLASTNAME}]",$default_setting['last_name'],$html_codes);
				$html_codes = str_replace("[{BCOMPANYNAME}]",$default_setting['company'],$html_codes);
				$html_codes = str_replace("[{BADDRESS}]",$default_setting['address'],$html_codes);
				$html_codes = str_replace("[{BTOWNCITY}]",$default_setting['city'],$html_codes);
				$html_codes = str_replace("[{BZIPCODE}]",$default_setting['zip_code'],$html_codes);
				$html_codes = str_replace("[{BCOUNTRY}]",$default_setting['country'],$html_codes);
				$html_codes = str_replace("[{BSTATE}]",$default_setting['state'],$html_codes);
				$html_codes = str_replace("[{SFIRSTNAME}]",$default_setting['first_name'],$html_codes);
				$html_codes = str_replace("[{SLASTNAME}]",$default_setting['last_name'],$html_codes);
				$html_codes = str_replace("[{SCOMPANYNAME}]",$default_setting['company'],$html_codes);
				$html_codes = str_replace("[{SADDRESS}]",$default_setting['address'],$html_codes);
				$html_codes = str_replace("[{STOWNCITY}]",$default_setting['city'],$html_codes);
				$html_codes = str_replace("[{SZIPCODE}]",$default_setting['zip_code'],$html_codes);
				$html_codes = str_replace("[{SCOUNTRY}]",$default_setting['country'],$html_codes);
				$html_codes = str_replace("[{PRODUCT}]",$default_setting['Product'],$html_codes);
				$html_codes = str_replace("[{QUNTITY}]",$default_setting['Quantity'],$html_codes);
				$html_codes = str_replace("[{PRICE}]",$default_setting['price'],$html_codes);
				$html_codes = str_replace("[{CARTSUBTOTAL}]",$default_setting['Cart_subtotal'],$html_codes);
				$html_codes = str_replace("[{SHIPPING}]",$default_setting['Shipping'],$html_codes);
				$html_codes = str_replace(" [{SSTATE}]",$default_setting['state'],$html_codes);
				$html_codes = str_replace("[{PAYMENTMETHOD}]",$default_setting['Payment_method'],$html_codes);
				$html_codes = str_replace("[{ORDERTOTAL}]",$default_setting['Order_total'],$html_codes);

				return $html_codes;
			}
			
			function replaced_order_processing_email_tempalte(){
			
				$default_setting = $this->def_setting();
				$default = $this->thankyou_email_settings();
				$option = get_option('aspk_processing_email_setting',$default);
				$html_codes =  html_entity_decode(stripslashes($option['new_mail_order']), 2, "UTF-8");
				$html_codes = str_replace("[{ORDERNO}]",$default_setting['order_no'],$html_codes);
				$html_codes = str_replace("[{EMAIL}]",$default_setting['email'],$html_codes);
				$html_codes = str_replace("[{TELENO}]",$default_setting['phone_no'],$html_codes);
				$html_codes = str_replace("[{BFIRSTNAME}]",$default_setting['first_name'],$html_codes);
				$html_codes = str_replace("[{BLASTNAME}]",$default_setting['last_name'],$html_codes);
				$html_codes = str_replace("[{BCOMPANYNAME}]",$default_setting['company'],$html_codes);
				$html_codes = str_replace("[{BADDRESS}]",$default_setting['address'],$html_codes);
				$html_codes = str_replace("[{BTOWNCITY}]",$default_setting['city'],$html_codes);
				$html_codes = str_replace("[{BZIPCODE}]",$default_setting['zip_code'],$html_codes);
				$html_codes = str_replace("[{BCOUNTRY}]",$default_setting['country'],$html_codes);
				$html_codes = str_replace("[{BSTATE}]",$default_setting['state'],$html_codes);
				$html_codes = str_replace("[{SFIRSTNAME}]",$default_setting['first_name'],$html_codes);
				$html_codes = str_replace("[{SLASTNAME}]",$default_setting['last_name'],$html_codes);
				$html_codes = str_replace("[{SCOMPANYNAME}]",$default_setting['company'],$html_codes);
				$html_codes = str_replace("[{SADDRESS}]",$default_setting['address'],$html_codes);
				$html_codes = str_replace("[{STOWNCITY}]",$default_setting['city'],$html_codes);
				$html_codes = str_replace("[{SZIPCODE}]",$default_setting['zip_code'],$html_codes);
				$html_codes = str_replace("[{SCOUNTRY}]",$default_setting['country'],$html_codes);
				$html_codes = str_replace("[{PRODUCT}]",$default_setting['Product'],$html_codes);
				$html_codes = str_replace("[{QUNTITY}]",$default_setting['Quantity'],$html_codes);
				$html_codes = str_replace("[{PRICE}]",$default_setting['price'],$html_codes);
				$html_codes = str_replace("[{CARTSUBTOTAL}]",$default_setting['Cart_subtotal'],$html_codes);
				$html_codes = str_replace("[{SHIPPING}]",$default_setting['Shipping'],$html_codes);
				$html_codes = str_replace(" [{SSTATE}]",$default_setting['state'],$html_codes);
				$html_codes = str_replace("[{PAYMENTMETHOD}]",$default_setting['Payment_method'],$html_codes);
				$html_codes = str_replace("[{ORDERTOTAL}]",$default_setting['Order_total'],$html_codes);
				return $html_codes;
			}
			
			function replaced_invoice_email_tempalte(){
			
				$default_setting = $this->def_setting();
				alog('$default_settinginvo',$default_setting,__FILE__,__LINE__);
				$default = $this->thankyou_email_settings();
				$option = get_option('aspk_invoice_email_setting',$default);
				$html_codes =  html_entity_decode(stripslashes($option['new_mail_order']), 2, "UTF-8");
				alog('$html_codes invo',$html_codes ,__FILE__,__LINE__);
				$html_codes = str_replace("[{ORDERNO}]",$default_setting['order_no'],$html_codes);
				$html_codes = str_replace("[{PAYLINK}]",$default_setting['pay_Link'],$html_codes);
				$html_codes = str_replace("[{DATE}]",$default_setting['date'],$html_codes);
				$html_codes = str_replace("[{PRODUCT}]",$default_setting['Product'],$html_codes);
				$html_codes = str_replace("[{QUNTITY}]",$default_setting['Quantity'],$html_codes);
				$html_codes = str_replace("[{PRICE}]",$default_setting['price'],$html_codes);
				$html_codes = str_replace("[{CARTSUBTOTAL}]",$default_setting['Cart_subtotal'],$html_codes);
				$html_codes = str_replace("[{SHIPPING}]",$default_setting['Shipping'],$html_codes);
				$html_codes = str_replace("[{PAYMENTMETHOD}]",$default_setting['Payment_method'],$html_codes);
				$html_codes = str_replace("[{ORDERTOTAL}]",$default_setting['Order_total'],$html_codes);
				return $html_codes;
			}
			
			function order_compelted_email_form(){
			
				if(isset($_POST['comp_ord_mail'])){
					$aspk_new_mail_order = $_POST['order_comp'];
					//$aspk_header = $_POST['comp_ord_sub'];
					//$aspk_recipant = $_POST['comp_ord_recp'];
					if(isset($_POST['comp_field'])){
						$aspk_chk = $_POST['comp_field'];
					}else{
						$aspk_chk = 'off';
					}
					$email_temp = array();
					$email_temp['new_mail_order'] = htmlentities($aspk_new_mail_order, 2, "UTF-8");
					//$email_temp['new_ord_sub'] = $aspk_header;
					//$email_temp['new_ord_recp'] = $aspk_recipant;
					$email_temp['check_field'] = $aspk_chk;
					update_option('aspk_my_order_completed_temp_settings', $email_temp);
					$replace_temp = $this->replaced_order_compeleted_email_tempalte();
				}
				$default_array = $this->thankyou_email_settings();
				$op = get_option('aspk_my_order_completed_temp_settings', $default_array);
				?>
									
					<div class="tw-bs container">
						<form action ="<?php echo admin_url('admin.php?page=aspk_customize_mails&tab=2');?>" method="post">		
							<div><?php if(isset($replace_temp)){echo $replace_temp;}?></div>
						<div class="row">
								<div class="col-md-12" style="margin-top:1em; background:white;">
									<div class="row">
										<div class="col-md-12" style="margin-top:1em;">
											Use Customize Template
										</div>				
									</div>
									<div class="row">
										<div class="col-md-12" style="margin-top:1em;">
											<input  type="checkbox" name="comp_field" <?php checked( $op['check_field'], 'on' ); ?>/>
										</div>				
									</div>	
									<!--<div class="row">
										<div class="col-md-12" style="margin-top:1em;">Email Subject</div>
									</div>
									<div class="row">
										<div class="col-md-12" style="margin-top:1em;">
											<input class="form-control" type="text" name="comp_ord_sub" value="<?php //if($op['new_ord_sub']){echo $op['new_ord_sub'];} ?>"/>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12" style="margin-top:1em;">Recipant</div>
									</div>
									<div class="row">
										<div class="col-md-12" style="margin-top:1em;">
											<input class="form-control" type="email" name="comp_ord_recp" value="<?php// if($op['new_ord_recp']){echo $op['new_ord_recp'];} ?>"/>
										</div>
									</div>-->
									<div  class="row" style="clear:left;">
										<div  class="col-md-12" style="margin-top:1em;">
											&nbsp;
										</div>
									</div>
								</div>
							</div>
							<div  class="row" style="clear:left;">
										<div  class="col-md-12" style="margin-top:1em;"></div>
							</div>
							<div class="row">
								<div class="col-md-12" style="margin-top:1em; background:white;">
									<div class="row">
										<div class="col-md-12">			
											<?php $this->aspk_order_comp();?>
										</div>
									</div>
									<div  class="row" style="clear:left;">
										<div  class="col-md-12" style="margin-top:1em;"></div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12" style="margin-top:1em; background:white;">
									<h4>Please use following Short Codes</h4>
									<div  class="row" style="clear:left;">
										<div  class="col-md-12" style="margin-top:1em;">
											<textarea style="width: 620px; height: 166px; background: none repeat scroll 0% 0% white;" readonly class="form-control">[{ORDERNO}] [{EMAIL}] [{TELENO}] [{BFIRSTNAME}] [{BLASTNAME}] [{BCOMPANYNAME}] [{BADDRESS}] [{BTOWNCITY}]	[{BZIPCODE}] [{BCOUNTRY}][{BSTATE}] [{SFIRSTNAME}] [{SLASTNAME}] [{SCOMPANYNAME}] [{SADDRESS}] [{STOWNCITY}] [{SZIPCODE}][{SCOUNTRY}]  [{CARTSUBTOTAL}] [{SHIPPING}] [{SSTATE}] [{ORDERTOTAL}]</textarea>
										</div>
									</div>
									<div  class="row" style="clear:left;">
										<div  class="col-md-12" style="margin-top:1em;">
										</div>
									</div>
								</div>
							</div>
							<div  class="row" style="clear:left;">
								<div  class="col-md-12" style="margin-top:1em;">
									<input class = "btn btn-primary" type="submit" value="Save Settings" name="comp_ord_mail">
								</div>
							</div>
							<div  class="row" style="clear:left;">
								<div  class="col-md-12" style="margin-top:1em;">
									&nbsp;
								</div>
							</div>
						</form>	
					</div>			
				<?php
			
			}
			
			
			
			function new_order_temp_settings(){
				
				if(isset($_POST['save_new_ord_mail'])){
					
					$default_setting = $this->def_setting();
					$aspk_new_mail_order = $_POST['new_mail_order'];
					$aspk_header = $_POST['new_ord_sub'];
					$aspk_recipant = $_POST['new_ord_recp'];
					if(isset($_POST['check_field'])){
						$aspk_chk = $_POST['check_field'];
					}else{
						$aspk_chk = 'off';
					}
					
					$email_temp = array();
					$email_temp['new_mail_order'] = htmlentities($aspk_new_mail_order, 2, "UTF-8");
					$email_temp['new_ord_sub'] = $aspk_header;
					$email_temp['new_ord_recp'] = $aspk_recipant;
					$email_temp['check_field'] = $aspk_chk;
					
					update_option('aspk_my_template_setting', $email_temp);
					$replace_temp = $this->replace_tempalte();
				}
					$default_array = $this->thankyou_email_settings();
					$op = get_option('aspk_my_template_setting', $default_array);
				?>
									
					<div class="tw-bs container">
						<form action ="<?php echo admin_url('admin.php?page=aspk_customize_mails&tab=0');?>" method="post">		
							<div><?php if(isset($replace_temp)){echo $replace_temp;}?></div>
							<div class="row">
								<div class="col-md-12" style="margin-top:1em; background:white;">
									<div class="row">
										<div class="col-md-12" style="margin-top:1em;">
											Use Customize Template
										</div>				
									</div>
									<div class="row">
										<div class="col-md-12" style="margin-top:1em;">
											<input  type="checkbox" name="check_field" <?php checked( $op['check_field'], 'on' ); ?>/>
										</div>				
									</div>	
									<div class="row">
										<div class="col-md-12" style="margin-top:1em;">Email Subject</div>
									</div>
									<div class="row">
										<div class="col-md-12" style="margin-top:1em;">
											<input class="form-control" type="text" name="new_ord_sub" value="<?php if($op['new_ord_sub']){echo $op['new_ord_sub'];} ?>"/>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12" style="margin-top:1em;">Recipant</div>
									</div>
									<div class="row">
										<div class="col-md-12" style="margin-top:1em;">
											<input class="form-control" type="email" name="new_ord_recp" value="<?php if($op['new_ord_recp']){echo $op['new_ord_recp'];} ?>"/>
										</div>
									</div>
									<div  class="row" style="clear:left;">
										<div  class="col-md-12">
											&nbsp;
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12" style="margin-top:1em; background:white;">
									<div class="row">
										<div class="col-md-12">			
											<?php $this->aspk_new_order();?>
										</div>
									</div>
									<div  class="row" style="clear:left;">
										<div  class="col-md-12" style="margin-top:1em;"></div>
									</div>
								</div>
							</div>	
							<div class="row">
								<div class="col-md-12" style="margin-top:1em; background:white;">
									<h4>Please use following Short Codes</h4>
									<div  class="row" style="clear:left;">
										<div  class="col-md-12" style="margin-top:1em;">
											<textarea style="width: 620px; height: 166px; background: none repeat scroll 0% 0% white;" readonly class="form-control">[{ORDERNO}] [{EMAIL}] [{TELENO}] [{BFIRSTNAME}] [{BLASTNAME}] [{BCOMPANYNAME}] [{BADDRESS}] [{BTOWNCITY}]	[{BZIPCODE}] [{BCOUNTRY}][{BSTATE}] [{SFIRSTNAME}] [{SLASTNAME}] [{SCOMPANYNAME}] [{SADDRESS}] [{STOWNCITY}] [{SZIPCODE}][{SCOUNTRY}]  [{CARTSUBTOTAL}] [{SHIPPING}] [{SSTATE}] [{ORDERTOTAL}]</textarea>
										</div>
									</div>
									<div  class="row" style="clear:left;">
										<div  class="col-md-12" style="margin-top:1em;">
										</div>
									</div>
								</div>
							</div>
							
							<div  class="row" style="clear:left;">
								<div  class="col-md-12" style="margin-top:1em;">
									<input class = "btn btn-primary" type="submit" value="Save Settings" name="save_new_ord_mail">
								</div>
							</div>
							<div  class="row" style="clear:left;">
								<div  class="col-md-12" style="margin-top:1em;">
									&nbsp;
								</div>
							</div>
						</form>	
					</div>			
				<?php
			
			}
			
			function customer_note(){
				
				if(isset($_POST['cust_note'])){
					
					$default_setting = $this->def_setting();
					$aspk_new_cust_note = $_POST['customer_note_editor'];
					//$aspk_header = $_POST['new_cust_sub'];
					//$aspk_recipant = $_POST['new_cust_recp'];
					if(isset($_POST['cust_check_field'])){
						$aspk_chk = $_POST['cust_check_field'];
					}else{
						$aspk_chk = 'off';
					}
					
					$email_temp = array();
					$email_temp['new_mail_order'] = htmlentities($aspk_new_cust_note , 2, "UTF-8");
					//$email_temp['new_ord_sub'] = $aspk_header;
					//$email_temp['new_ord_recp'] = $aspk_recipant;
					$email_temp['check_field'] = $aspk_chk;
					
					update_option('aspk_note_template_setting', $email_temp);
					$replace_temp = $this->replace_note_email();
				}
					$default_array = $this->thankyou_email_settings();
					$op = get_option('aspk_note_template_setting', $default_array);
				?>
									
					<div class="tw-bs container">
						<form action ="<?php echo admin_url('admin.php?page=aspk_customize_mails&tab=6');?>" method="post">		
							<div><?php if(isset($replace_temp)){echo $replace_temp;}?></div>
							<div class="row">
								<div class="col-md-12" style="margin-top:1em; background:white;">
									<div class="row">
										<div class="col-md-12" style="margin-top:1em;">
											Use Customize Template
										</div>				
									</div>
									<div class="row">
										<div class="col-md-12" style="margin-top:1em;">
											<input  type="checkbox" name="cust_check_field" <?php checked( $op['check_field'], 'on' ); ?>/>
										</div>				
									</div>	
									<!--<div class="row">
										<div class="col-md-12" style="margin-top:1em;">Email Subject</div>
									</div>
									<div class="row">
											<input class="form-control" type="text" name="new_cust_sub" value="<?php //if($op['new_ord_sub']){echo $op['new_ord_sub'];} ?>"/>
										<div class="col-md-12" style="margin-top:1em;">
										</div>
									</div>
									<div class="row">
										<div class="col-md-12" style="margin-top:1em;">Recipant</div>
									</div>
									<div class="row">
										<div class="col-md-12" style="margin-top:1em;">
											<input class="form-control" type="email" name="new_cust_recp" value="<?php //if($op['new_ord_recp']){echo $op['new_ord_recp'];} ?>"/>
										</div>
									</div>-->
									<div  class="row" style="clear:left;">
										<div  class="col-md-12">
											&nbsp;
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12" style="margin-top:1em; background:white;">
									<div class="row">
										<div class="col-md-12">			
											<?php $this->customer_note_editor();?>
										</div>
									</div>
									<div  class="row" style="clear:left;">
										<div  class="col-md-12" style="margin-top:1em;"></div>
									</div>
								</div>
							</div>	
							<div class="row">
								<div class="col-md-12" style="margin-top:1em; background:white;">
									<h4>Please use following Short Codes</h4>
									<div  class="row" style="clear:left;">
										<div  class="col-md-12" style="margin-top:1em;">
											<textarea style="width: 620px; height: 166px; background: none repeat scroll 0% 0% white;" readonly class="form-control">[{ORDERNO}] [{EMAIL}] [{TELENO}] [{BFIRSTNAME}] [{BLASTNAME}] [{BCOMPANYNAME}] [{BADDRESS}] [{BTOWNCITY}]	[{BZIPCODE}] [{BCOUNTRY}][{BSTATE}] [{SFIRSTNAME}] [{SLASTNAME}] [{SCOMPANYNAME}] [{SADDRESS}] [{STOWNCITY}] [{SZIPCODE}][{SCOUNTRY}]  [{CARTSUBTOTAL}] [{SHIPPING}] [{SSTATE}] [{ORDERTOTAL}]</textarea>
										</div>
									</div>
									<div  class="row" style="clear:left;">
										<div  class="col-md-12" style="margin-top:1em;">
										</div>
									</div>
								</div>
							</div>
							
							<div  class="row" style="clear:left;">
								<div  class="col-md-12" style="margin-top:1em;">
									<input class = "btn btn-primary" type="submit" value="Save Settings" name="cust_note">
								</div>
							</div>
							<div  class="row" style="clear:left;">
								<div  class="col-md-12" style="margin-top:1em;">
									&nbsp;
								</div>
							</div>
						</form>	
					</div>			
				<?php
			
			}
			
			function order_processing_email_settings(){
				if(isset($_POST['save_pro_ord_mail'])){
					
					$default_setting = $this->def_setting();
					$aspk_new_mail_order = $_POST['pro_mail_order'];
					$aspk_header = $_POST['pro_ord_sub'];
					$aspk_recipant = $_POST['pro_ord_recp'];
					if(isset($_POST['pro_check_field'])){
						$aspk_chk = $_POST['pro_check_field'];
					}else{
						$aspk_chk = 'off';
					}
					
					$email_temp = array();
					$email_temp['new_mail_order'] = htmlentities($aspk_new_mail_order, 2, "UTF-8");
					$email_temp['new_ord_sub'] = $aspk_header;
					$email_temp['new_ord_recp'] = $aspk_recipant;
					$email_temp['check_field'] = $aspk_chk;
					
					update_option('aspk_processing_email_setting', $email_temp);
					$replace_temp = $this->replaced_order_processing_email_tempalte();
				}
					$default_array = $this->thankyou_email_settings();
					$op = get_option('aspk_processing_email_setting', $default_array);
				?>
									
					<div class="tw-bs container">
						<form action ="<?php echo admin_url('admin.php?page=aspk_customize_mails&tab=1');?>" method="post">		
							<div><?php if(isset($replace_temp)){echo $replace_temp;}?></div>
							<div class="row">
								<div class="col-md-12" style="margin-top:1em; background:white;">
									<div class="row">
										<div class="col-md-12" style="margin-top:1em;">
											Use Customize Template
										</div>				
									</div>
									<div class="row">
										<div class="col-md-12" style="margin-top:1em;">
											<input  type="checkbox" name="pro_check_field" <?php checked( $op['check_field'], 'on' ); ?>/>
										</div>				
									</div>	
									<!--<div class="row">
										<div class="col-md-12" style="margin-top:1em;">Email Subject</div>
									</div>
									<div class="row">
										<div class="col-md-12" style="margin-top:1em;">
											<input class="form-control" type="text" name="pro_ord_sub" value="<?php //if($op['new_ord_sub']){echo $op['new_ord_sub'];} ?>"/>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12" style="margin-top:1em;">Recipant</div>
									</div>
									<div class="row">
										<div class="col-md-12" style="margin-top:1em;">
											<input class="form-control" type="email" name="pro_ord_recp" value="<?php// if($op['new_ord_recp']){echo $op['new_ord_recp'];} ?>"/>
										</div>
									</div>-->
									<div  class="row" style="clear:left;">
										<div  class="col-md-12" style="margin-top:1em;">
											&nbsp;
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12" style="margin-top:1em; background:white;">
									<div class="row">
										<div class="col-md-12">			
											<?php $this->processing_order_editor();?>
										</div>
									</div>
									<div  class="row" style="clear:left;">
										<div  class="col-md-12" style="margin-top:1em;"></div>
									</div>
								</div>
							</div>	
							<div class="row">
								<div class="col-md-12" style="margin-top:1em; background:white;">
									<h4>Please use following Short Codes</h4>
									<div  class="row" style="clear:left;">
										<div  class="col-md-12" style="margin-top:1em;">
											<textarea style="width: 620px; height: 166px; background: none repeat scroll 0% 0% white;" readonly class="form-control">[{ORDERNO}] [{EMAIL}] [{TELENO}] [{BFIRSTNAME}] [{BLASTNAME}] [{BCOMPANYNAME}] [{BADDRESS}] [{BTOWNCITY}]	[{BZIPCODE}] [{BCOUNTRY}][{BSTATE}] [{SFIRSTNAME}] [{SLASTNAME}] [{SCOMPANYNAME}] [{SADDRESS}] [{STOWNCITY}] [{SZIPCODE}][{SCOUNTRY}]  [{CARTSUBTOTAL}] [{SHIPPING}] [{SSTATE}] [{ORDERTOTAL}]</textarea>
										</div>
									</div>
									<div  class="row" style="clear:left;">
										<div  class="col-md-12" style="margin-top:1em;">
										</div>
									</div>
								</div>
							</div>
							<div  class="row" style="clear:left;">
								<div  class="col-md-12" style="margin-top:1em;">
									<input class = "btn btn-primary" type="submit" value="Save Settings" name="save_pro_ord_mail">
								</div>
							</div>
							<div  class="row" style="clear:left;">
								<div  class="col-md-12" style="margin-top:1em;">
									&nbsp;
								</div>
							</div>
						</form>	
					</div>			
				<?php
			}
			
			function invoice_email_settings(){
				if(isset($_POST['save_invoice_ord_mail'])){
					
					$default_setting = $this->def_setting();
					$aspk_new_mail_order = $_POST['invoice_mail_order'];
					$aspk_header = $_POST['invoice_ord_sub'];
					$aspk_recipant = $_POST['invoice_ord_recp'];
					if(isset($_POST['invoice_check_field'])){
						$aspk_chk = $_POST['invoice_check_field'];
					}else{
						$aspk_chk = 'off';
					}
					
					$email_temp = array();
					$email_temp['new_mail_order'] = htmlentities($aspk_new_mail_order, 2, "UTF-8");
					$email_temp['new_ord_sub'] = $aspk_header;
					$email_temp['new_ord_recp'] = $aspk_recipant;
					$email_temp['check_field'] = $aspk_chk;
					
					update_option('aspk_invoice_email_setting', $email_temp);
					$replace_temp = $this->replaced_invoice_email_tempalte();
				}
					$default_array = $this->thankyou_email_settings();
					$op = get_option('aspk_invoice_email_setting', $default_array);
				?>
									
					<div class="tw-bs container">
						<form action ="<?php echo admin_url('admin.php?page=aspk_customize_mails&tab=3');?>" method="post">		
							<div><?php if(isset($replace_temp)){echo $replace_temp;}?></div>
							<div class="row">
								<div class="col-md-12" style="margin-top:1em; background:white;">
								<div class="row">
										<div class="col-md-12" style="margin-top:1em;">
											Use Customize Template
										</div>				
									</div>
									<div class="row">
										<div class="col-md-12" style="margin-top:1em;">
											<input  type="checkbox" name="invoice_check_field" <?php checked( $op['check_field'], 'on' ); ?>/>
										</div>				
									</div>	
									<!--<div class="row">
										<div class="col-md-12" style="margin-top:1em;">Email Subject</div>
									</div>
									<div class="row">
										<div class="col-md-12" style="margin-top:1em;">
											<input class="form-control" type="text" name="invoice_ord_sub" value="<?php// if($op['new_ord_sub']){echo $op['new_ord_sub'];} ?>"/>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12" style="margin-top:1em;">Recipant</div>
									</div>
									<div class="row">
										<div class="col-md-12" style="margin-top:1em;">
											<input class="form-control" type="email" name="invoice_ord_recp" value="<?php //if($op['new_ord_recp']){echo $op['new_ord_recp'];} ?>"/>
										</div>
									</div>-->
									<div  class="row" style="clear:left;">
										<div  class="col-md-12" style="margin-top:1em;">
											&nbsp;
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12" style="margin-top:1em; background:white;">
									<div class="row">
										<div class="col-md-12">			
											<?php $this->invoice_order_editor();?>
										</div>
									</div>
									<div  class="row" style="clear:left;">
										<div  class="col-md-12" style="margin-top:1em;"></div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12" style="margin-top:1em; background:white;">
									<h4>Please use following Short Codes</h4>
									<div class="row">
										<div  class="col-md-12" style="margin-top:1em;">
											<textarea style="width: 620px; height: 166px; background: none repeat scroll 0% 0% white;" readonly class="form-control">[{ORDERNO}] [[{SHIPPING}]  [{DATE}] [{CARTSUBTOTAL}] </textarea>
										</div>
									</div>
									<div  class="row" style="clear:left;">
										<div  class="col-md-12" style="margin-top:1em;"></div>
									</div>
								</div>
								
							</div>
							<div  class="row" style="clear:left;">
								<div  class="col-md-12" style="margin-top:1em;">
									<input class = "btn btn-primary" type="submit" value="Save Settings" name="save_invoice_ord_mail">
								</div>
							</div>
							<div  class="row" style="clear:left;">
								<div  class="col-md-12" style="margin-top:1em;">
									&nbsp;
								</div>
							</div>
						</form>	
					</div>			
				<?php
			}
			
			function customer_new_account_email_settings(){
				if(isset($_POST['save_new_account_ord_mail'])){
					
					$default_setting = $this->def_setting();
					$aspk_new_mail_order = $_POST['new_account_mail_order'];
					$aspk_header = $_POST['new_account_ord_sub'];
					$aspk_recipant = $_POST['new_account_ord_recp'];
					if(isset($_POST['new_account_check_field'])){
						$aspk_chk = $_POST['new_account_check_field'];
					}else{
						$aspk_chk = 'off';
					}
					
					$email_temp = array();
					$email_temp['new_mail_order'] = htmlentities($aspk_new_mail_order, 2, "UTF-8");
					$email_temp['new_ord_sub'] = $aspk_header;
					$email_temp['new_ord_recp'] = $aspk_recipant;
					$email_temp['check_field'] = $aspk_chk;
					
					update_option('aspk_new_account_email_setting', $email_temp);
					$replace_temp = $this->replace_new_account_tempalte();
				}
					$default_array = $this->thankyou_email_settings();
					$op = get_option('aspk_new_account_email_setting', $default_array);
				?>
									
					<div class="tw-bs container">
						<form action ="<?php echo admin_url('admin.php?page=aspk_customize_mails&tab=4');?>" method="post">		
							<div><?php if(isset($replace_temp)){echo $replace_temp;}?></div>
							<div class="row">
								<div class="col-md-12" style="margin-top:1em; background:white;">
									<div class="row">
										<div class="col-md-12" style="margin-top:1em;">
											Use Customize Template
										</div>				
									</div>
									<div class="row">
										<div class="col-md-12" style="margin-top:1em;">
											<input  type="checkbox" name="new_account_check_field" <?php checked( $op['check_field'], 'on' ); ?>/>
										</div>				
									</div>	
									<!--<div class="row">
										<div class="col-md-12" style="margin-top:1em;">Email Subject</div>
									</div>
									<div class="row">
										<div class="col-md-12" style="margin-top:1em;">
											<input class="form-control" type="text" name="new_account_ord_sub" value="<?php //if($op['new_ord_sub']){echo $op['new_ord_sub'];} ?>"/>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12" style="margin-top:1em;">Recipant</div>
									</div>
									<div class="row">
										<div class="col-md-12" style="margin-top:1em;">
											<input class="form-control" type="email" name="new_account_ord_recp" value="<?php //if($op['new_ord_recp']){echo $op['new_ord_recp'];} ?>"/>
										</div>
									</div>-->
									<div  class="row" style="clear:left;">
										<div  class="col-md-12" style="margin-top:1em;">
											&nbsp;
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12" style="margin-top:1em; background:white;">
									<div class="row">
										<div class="col-md-12">			
											<?php $this->new_account_order_editor();?>
										</div>
									</div>
									<div  class="row" style="clear:left;">
										<div  class="col-md-12" style="margin-top:1em;"></div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12" style="margin-top:1em; background:white;">
									<h4>Please use following Short Codes</h4>
									<div  class="row" style="clear:left;">
										<div  class="col-md-12" style="margin-top:1em;">
											<textarea style="width: 620px; height: 166px; background: none repeat scroll 0% 0% white;" readonly class="form-control">[{USERNAME}] [{LINK}]</textarea>
										</div>
									</div>
									<div  class="row" style="clear:left;">
										<div  class="col-md-12" style="margin-top:1em;"></div>
									</div>
								</div>
							</div>
							<div  class="row" style="clear:left;">
								<div  class="col-md-12" style="margin-top:1em;">
									<input class = "btn btn-primary" type="submit" value="Save Settings" name="save_new_account_ord_mail">
								</div>
							</div>
							<div  class="row" style="clear:left;">
								<div  class="col-md-12" style="margin-top:1em;">
									&nbsp;
								</div>
							</div>
						</form>	
					</div>			
				<?php
			}
			
			function customer_reset_pass_email_settings(){
				 if(isset($_POST['save_reset_mail'])){
					
					$default_setting = $this->def_setting();
					$aspk_new_mail_order = $_POST['reset_pass_editor'];
					$aspk_header = $_POST['reset_sub'];
					$aspk_recipant = $_POST['reset_recp'];
					if(isset($_POST['reset_check_field'])){
						$aspk_chk = $_POST['reset_check_field'];
					}else{
						$aspk_chk = 'off';
					}
					
					$email_temp = array();
					$email_temp['new_mail_order'] = htmlentities($aspk_new_mail_order, 2, "UTF-8");
					$email_temp['new_ord_sub'] = $aspk_header;
					$email_temp['new_ord_recp'] = $aspk_recipant;
					$email_temp['check_field'] = $aspk_chk;
					
					update_option('aspk_reset_email_setting', $email_temp);
					$replace_temp = $this->replace_reset_password_tempalte();
				} 
					$default_array = $this->thankyou_email_settings();
					$op = get_option('aspk_reset_email_setting', $default_array); 
				?>	
					<div class="tw-bs container">
						<form action ="<?php echo admin_url('admin.php?page=aspk_customize_mails&tab=5');?>" method="post">		
						<div><?php if(isset($replace_temp)){echo $replace_temp;}?></div>
							<div class="row">
								<div class="col-md-12" style="margin-top:1em; background:white;">
									<div class="row">
										<div class="col-md-12" style="margin-top:1em;">
											Use Customize Template
										</div>				
									</div>
								<div class="row">
									<div class="col-md-12" style="margin-top:1em;">
										<input  type="checkbox" name="reset_check_field" <?php checked( $op['check_field'], 'on' ); ?>/>
									</div>				
								</div>	
								<!--<div class="row">
									<div class="col-md-12" style="margin-top:1em;">Email Subject</div>
								</div>
								<div class="row">
									<div class="col-md-12" style="margin-top:1em;">
										<input class="form-control" type="text" name="reset_sub" value="<?php //if($op['new_ord_sub']){echo $op['new_ord_sub'];} ?>"/>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12" style="margin-top:1em;">Recipant</div>
								</div>
								<div class="row">
									<div class="col-md-12" style="margin-top:1em;">
										<input class="form-control" type="email" name="reset_recp" value="<?php //if($op['new_ord_recp']){echo $op['new_ord_recp'];} ?>"/>
									</div>
								</div>-->
								<div  class="row" style="clear:left;">
									<div  class="col-md-12" style="margin-top:1em;">
										&nbsp;
									</div>
								</div>
							</div>
						</div>
						<div class="row">
								<div class="col-md-12" style="margin-top:1em; background:white;">
									<div class="row">
										<div class="col-md-12">			
											<?php  $this->reset_password_editor();?>
										</div>
									</div>
									<div  class="row" style="clear:left;">
										<div  class="col-md-12" style="margin-top:1em;"></div>
									</div>
								</div>
						</div>
							<div class="row">
								<div class="col-md-12" style="margin-top:1em; background:white;">
									<h4>Please use following Short Codes</h4>
									<div  class="row" style="clear:left;">
										<div  class="col-md-12" style="margin-top:1em;">
											<textarea style="width: 620px; height: 166px; background: none repeat scroll 0% 0% white;" readonly class="form-control">[{USERNAME}] [{LINK}]</textarea>
										</div>
									</div>
									<div  class="row" style="clear:left;">
										<div  class="col-md-12" style="margin-top:1em;"></div>
									</div>
								</div>
							</div>
						<div  class="row" style="clear:left;">
							<div  class="col-md-12" style="margin-top:1em;">
								<input class = "btn btn-primary" type="submit" value="Save Settings" name="save_reset_mail">
							</div>
						</div>
						<div  class="row" style="clear:left;">
							<div  class="col-md-12" style="margin-top:1em;">
								&nbsp;
							</div>
						</div>
					</form>	
				</div>
				<?php
			}
			
			function Customize_Emails(){
				if(isset($_GET['tab'])){
					$tab = $_GET['tab'];
				}else{
					$tab = 0;
				}
				?>
				<div id="tabs" style="width:80%;">
					<ul>
						<li><a href="#tabs-1">New order</a></li>
						<li><a href="#tabs-2">Processing Order</a></li>
						<li><a href="#tabs-3">Customer Completed Order</a></li>
						<li><a href="#tabs-4">Customer Invoice</a></li>
						<li><a href="#tabs-5">Customer New Account</a></li>
						<li><a href="#tabs-6">Reset Password</a></li>
						<li><a href="#tabs-7">Customer Note</a></li>
					</ul>
					<div id="tabs-1">
							<?php $this->new_order_temp_settings(); ?>
					</div>
					<div id="tabs-2">
							<?php $this->order_processing_email_settings(); ?>
					</div>
					<div id="tabs-3">
						<?php $this->order_compelted_email_form(); ?>
					</div>
					<div id="tabs-4">
						<?php $this->invoice_email_settings(); ?>
					</div>
					<div id="tabs-5">
						<?php $this->customer_new_account_email_settings(); ?>
					</div>
					<div id="tabs-6">
						<?php $this->customer_reset_pass_email_settings(); ?>
					</div>
					<div id="tabs-7">
						<?php $this->customer_note();?>
					</div>
				</div>
				
				<script>
				   jQuery(function() {
				   jQuery( "#tabs" ).tabs({active: <?php echo $tab ;?>});
				  });
				</script>	
				
				<?php		
		
			}		
			
		}		
	}
$aspk_wc_emails = new aspk_wc_customize_emails();