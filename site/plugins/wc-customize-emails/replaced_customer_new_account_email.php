<?php
	global $aspk_wc_emails;
	$default_array = $aspk_wc_emails->thankyou_email_settings();
	$option = get_option('aspk_new_account_email_setting',$default_array);
	alog('$option',$option,__FILE__,__LINE__);
	
	$html_codes =  html_entity_decode(stripslashes($option['new_mail_order']), 2, "UTF-8");
	alog('$html_codes',$html_codes,__FILE__,__LINE__);
	alog('$blogname',$blogname,__FILE__,__LINE__);
	alog('$blogname222',get_permalink( wc_get_page_id( 'myaccount' ) ),__FILE__,__LINE__);
	$html_codes = str_replace("[{USERNAME}]",esc_html( $blogname ),$html_codes);
	$html_codes = str_replace("[{LINK}]",get_permalink( wc_get_page_id( 'myaccount' ) ),$html_codes);
	echo $html_codes;

?>