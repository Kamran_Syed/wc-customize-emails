<div style="background-color:#f5f5f5;width:100%;margin:0;">
<div><?php  get_header();?></div>
        	<table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%"><tbody><tr><td align="center" valign="top">
                    	<table border="0" cellpadding="0" cellspacing="0" width="600" style="border-radius:6px!important;background-color:#fdfdfd;border:1px solid #dcdcdc;border-radius:6px!important"><tbody><tr><td align="center" valign="top">
                                    
                                	<table border="0" cellpadding="0" cellspacing="0" width="600" style="background-color:#557da1;color:#ffffff;border-top-left-radius:6px!important;border-top-right-radius:6px!important;border-bottom:0;font-family:Arial;font-weight:bold;line-height:100%;vertical-align:middle" bgcolor="#557da1"><tbody><tr><td>
                                            	<h1 style="color:#ffffff;margin:0;padding:28px 24px;display:block;font-family:Arial;font-size:30px;font-weight:bold;text-align:left;line-height:150%;">Invoice for order [{ORDERNO}]</h1>

                                            </td>
                                        </tr></tbody></table></td>
                            </tr><tr><td align="center" valign="top">
                                    
                                	<table border="0" cellpadding="0" cellspacing="0" width="600"><tbody><tr><td valign="top" style="background-color:#fdfdfd;border-radius:6px!important">
                                                
                                                <table border="0" cellpadding="20" cellspacing="0" width="100%"><tbody><tr><td valign="top">
                                                            <div style="padding: 1em; color:#737373;font-family:Arial;font-size:14px;line-height:150%;text-align:left;">


	<p>An order has been created for you on QA19. To pay for this order
please use the following link:[{PAYLINK}]



<h2>Order:[{ORDERNO}]([{DATE}])</h2>

<table cellspacing="0" cellpadding="6" style="width:100%;border:1px solid #eee" border="1"><thead>
<tr><th scope="col" style="text-align:left;border:1px solid #eee">Product</th>
			<th scope="col" style="text-align:left;border:1px solid #eee">Quantity</th>
			<th scope="col" style="text-align:left;border:1px solid #eee">Price</th>
		</tr></thead>
	<tbody>
		<tr><td style="border: 1px solid #eee;"colspan="3">[{items}]</td></tr>
		
	</tbody>
	<tfoot><tr><th scope="row" colspan="2" style="text-align:left;border:1px solid #eee;border-top-width:4px">Cart Subtotal:</th>
						<td style="text-align:left;border:1px solid #eee;border-top-width:4px"><span>[{CARTSUBTOTAL}]</span></td>
					</tr><tr><th scope="row" colspan="2" style="text-align:left;border:1px solid #eee">Shipping:</th>
						<td style="text-align:left;border:1px solid #eee">[{SHIPPING}]</td>
					</tr><tr><th scope="row" colspan="2" style="text-align:left;border:1px solid #eee">Payment Method:</th>
						<td style="text-align:left;border:1px solid #eee">[{PAYMENTMETHOD}]</td>
					</tr><tr><th scope="row" colspan="2" style="text-align:left;border:1px solid #eee">Order Total:</th>
						<td style="text-align:left;border:1px solid #eee"><span>[{ORDERTOTAL}]</span></td	</tr></tfoot></table></div>
														</td>
                                                    </tr></tbody></table></td>
                                        </tr></tbody></table></td>
                            </tr><tr><td align="center" valign="top">
                                    
                                	<table border="0" cellpadding="10" cellspacing="0" width="600" style="border-top:0"><tbody><tr><td valign="top">
                                                <table border="0" cellpadding="10" cellspacing="0" width="100%"><tbody><tr><td colspan="2" valign="middle" style="border:0;color:#99b1c7;font-family:Arial;font-size:12px;line-height:125%;text-align:center">
                                                        	<p>QA19 –
Powered by WooCommerce</p>
                                                        </td>
                                                    </tr></tbody></table></td>
                                        </tr></tbody></table></td>
                            </tr></tbody></table></td>
                </tr></tbody></table></div>
				<div><?php  get_footer();?></div>