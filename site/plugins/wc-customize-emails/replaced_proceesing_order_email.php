<?php
	global $aspk_wc_emails;
	$default_array = $aspk_wc_emails->thankyou_email_settings();
	$option = get_option('aspk_processing_email_setting',$default_array);
	alog('$optionprocessing',$option,__FILE__,__LINE__);
	$get_order = new WC_Order($order->post->ID);
	$items = $get_order->get_items();
	$ord_detail = $order->get_order_item_totals();
	///////////////////////////////////////////////////////////////////
	$html_codes =  html_entity_decode(stripslashes($option['new_mail_order']), 2, "UTF-8");
	alog('$html_codesprocessing',$html_codes,__FILE__,__LINE__);
	$html_codes = str_replace("[{ORDERNO}]",$order->get_order_number(),$html_codes);
	$html_codes = str_replace("[{EMAIL}]",$order->billing_email,$html_codes);
	$html_codes = str_replace("[{TELENO}]",$order->billing_phone,$html_codes);
	//////////////////////////////////////////////////////////////////////////////
	$html_codes = str_replace("[{items}]",$order->email_order_items_table( $order->is_download_permitted(), true, $order->has_status( 'processing') ),$html_codes);
	$html_codes = str_replace("[{ORDERTOTAL}]",$ord_detail['order_total']['value'],$html_codes);
	$html_codes = str_replace("[{CARTSUBTOTAL}]",$ord_detail['cart_subtotal']['value'],$html_codes);
	if(isset($ord_detail['shipping']['value'])){
		$html_codes = str_replace("[{SHIPPING}]",$ord_detail['shipping']['value'],$html_codes);
	}else{
		$html_codes = str_replace("[{SHIPPING}]","",$html_codes);
	}
	if(isset($ord_detail['payment_method']['value'])){
		$html_codes = str_replace("[{PAYMENTMETHOD}]",$ord_detail['payment_method']['value'],$html_codes);
	}else{
		$html_codes = str_replace("[{PAYMENTMETHOD}]","",$html_codes);
	}
	///////////////////////////////////////////////////////////////////////////
	
	$html_codes = str_replace("[{BFIRSTNAME}]",$order->billing_first_name,$html_codes);
	$html_codes = str_replace("[{BLASTNAME}]",$order->billing_last_name,$html_codes);
	$html_codes = str_replace("[{BCOMPANYNAME}]",$order->billing_company,$html_codes);
	$html_codes = str_replace("[{BADDRESS}]",$order->billing_address_1,$html_codes);
	$html_codes = str_replace("[{BTOWNCITY}]",$order->billing_city,$html_codes);
	$html_codes = str_replace("[{BZIPCODE}]",$order->billing_postcode,$html_codes);
	$html_codes = str_replace("[{BCOUNTRY}]",$order->billing_country,$html_codes);
	$html_codes = str_replace("[{BSTATE}]",$order->billing_state,$html_codes);
	//////////////////////////////////////////////////////////////////////////

	$html_codes = str_replace("[{SFIRSTNAME}]",$order->shipping_first_name,$html_codes);
	$html_codes = str_replace("[{SLASTNAME}]",$order->shipping_last_name,$html_codes);
	$html_codes = str_replace("[{SCOMPANYNAME}]",$order->shipping_company,$html_codes);
	$html_codes = str_replace("[{SADDRESS}]",$order->shipping_address_1,$html_codes);
	$html_codes = str_replace("[{STOWNCITY}]",$order->shipping_city,$html_codes);
	$html_codes = str_replace("[{SZIPCODE}]",$order->shipping_postcode,$html_codes);
	$html_codes = str_replace("[{SCOUNTRY}]",$order->shipping_country,$html_codes);
	$html_codes = str_replace("[{SSTATE}]",$order->shipping_state,$html_codes);
	echo $html_codes;

?>