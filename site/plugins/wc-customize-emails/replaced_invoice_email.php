<?php
	global $aspk_wc_emails;
	$default_array = $aspk_wc_emails->thankyou_email_settings();
	$option = get_option('aspk_invoice_email_setting',$default_array);
	alog('$optioninvoice',$option,__FILE__,__LINE__);
	$get_order = new WC_Order($order->post->ID);
	$items = $get_order->get_items();
	$ord_detail = $order->get_order_item_totals();
	
	$html_codes =  html_entity_decode(stripslashes($option['new_mail_order']), 2, "UTF-8");	
	alog('$html_inovice',$html_codes,__FILE__,__LINE__);
	$html_codes = str_replace("[{items}]",$order->email_order_items_table( $order->is_download_permitted(), true, $order->has_status( 'pending') ),$html_codes);
	
	$html_codes = str_replace("[{ORDERNO}]",$order->get_order_number(),$html_codes);
	
	$html_codes = str_replace("[{DATE}]",date_i18n( wc_date_format()),$html_codes);
	
	$html_codes = str_replace("[{ORDERTOTAL}]",$ord_detail['order_total']['value'],$html_codes);
	$html_codes = str_replace("[{CARTSUBTOTAL}]",$ord_detail['cart_subtotal']['value'],$html_codes);
	if(isset($ord_detail['shipping']['value'])){
		$html_codes = str_replace("[{SHIPPING}]",$ord_detail['shipping']['value'],$html_codes);
	}else{
		$html_codes = str_replace("[{SHIPPING}]","",$html_codes);
	}
	if(isset($ord_detail['payment_method']['value'])){
		$html_codes = str_replace("[{PAYMENTMETHOD}]",$ord_detail['payment_method']['value'],$html_codes);
	}else{
		$html_codes = str_replace("[{PAYMENTMETHOD}]","",$html_codes);
	}
	$html_codes = str_replace("[{PAYLINK}]", '<a href='.esc_url( $order->get_checkout_payment_url() ).'>' . __( 'pay', 'woocommerce' ) .  '</a>',$html_codes);
	echo $html_codes;

?>